# Bio AGgressor Ontology (BAGO) creation method using cellfie

bago_vide.owl contains the metadata description of BAGO and also all its object properties.

Delete the catalog-v001.xml file to remove previous references to any semantic ressources in Protege.

## Classes Creation
Open file bago_vide.owl with Protege.
Check the preferences in Protege
	Renderer by Entity IRI short Name
 	New entities Entity Iri
	 	start with: Active ontology IRI
	 	followed by : #
Create the classes using cellfie with the file Bioagresseurs_onto.xls and the ClassDefinition rule.
Save  the new version  as bago_onto_latest.owl

Add some documentation notes of those classes using cellfie with the file Bioagresseurs_onto.xls and the ClassDocumentation rule.

## Genetic Ressource Classes

### Create the taxons from NCBI taxons
Check the preferences in Protege of renderer by entity IRI short name (id)
Change the preferences in Protege of new entity
	Entity Iri
	 	start with specified IRI: http://purl.bioontology.org/ontology/NCBITAXON
	 	followed by : /
Create the class using cellfie with the file Bioagresseurs_onto.xls and the NcbiClassDefinition rule.


### Create the taxons from TAXREF-LD
Change the preferences in Protege
	Entity Iri
	 	start with: http://taxref.mnhn.fr/lod/taxon
	 	followed by : /
Create the class using cellfie with the file Bioagresseurs_onto.xls and the TaxrefClassDefinition rule.
 

### Create the class fcuo:Crop
Change in the Protege renderer the configuration of new entity
	Entity Iri
	 	start with: https://opendata.inrae.fr/fcu-def
	 	followed by : #


## Add the constraints and their definitions for some classes

Check the preferences in Protege
	Renderer by Entity IRI prefix name

	complete the slot Equivalent To of the class description, using the class expression editor in Protege
	see the definitions tab in the file Bioagresseurs_onto.xls

Add the domain and range constraint of objects property
see the Property tab in the file Bioagresseurs_onto.xls
