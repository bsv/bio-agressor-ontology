# Specification: Competency Questions (CQ).
* SPARQL EndPoint : https://rdf.codex.cati.inrae.fr/bag-res/grapevine/sparql
* SPARQL EndPoint : https://rdf.codex.cati.inrae.fr/bag-def/sparql

## Information about living organisms

CQ: What is the scientific taxon of a living organism O (O could be a bioaggressor or a cultivated plant)? 
"Daktulosphaira vitifoliae" is an animal, "grapevine" is a plant,  etc.

CQ: What are the species scientific names (preferred and known synonyms) of the living organism O (bioaggressor or cultivated plant)? 
cultivated grapevine species is "Vitis vinifera", grape phylloxera species is "Daktulosphaira vitifoliae", etc.

CQ: What are the vernacular names of the living organism  O (bioagressor or cultivated plant)? 
"Vitis vinifera" vernacular name is "cultivated grapevine", "Daktulosphaira vitifoliae" vernacular name is "grape phylloxera", etc.

### CQ: What are the bacteria, their scientific names and vernacular names
Quelles sont les bactéries, leurs noms scientifiques et vernaculaires s'ils existent

```
SELECT DISTINCT ?bacterium ?vernacular ?sc_name ?synonym
{ 
	?bacterium rdf:type bago:Bacterium.
	OPTIONAL {
	          ?bacterium skos:altLabel ?vernacular.
	           FILTER ( lang(?vernacular) = "fr" )
	           }
	?bacterium bago:isDescribedBy ?taxon.
	?taxon skos:prefLabel ?sc_name.
        OPTIONAL {
                 ?taxon skos:altLabel ?synonym.
                 FILTER ( lang(?synonym) = "fr" )
                 }
	FILTER ( lang(?sc_name) = "fr" )
} 
LIMIT 50
```
### CQ: What are the virus, their scientific names and vernacular names
Quels sont les virus, leurs noms scientifiques et vernaculaires s'ils existent

```
SELECT DISTINCT ?virus ?vernacular ?sc_name ?synonym
{ 
	?virus rdf:type bago:Virus.
	 OPTIONAL {
	          ?virus skos:altLabel ?vernacular.
	           FILTER ( lang(?vernacular) = "fr" )
	           }
	?virus bago:isDescribedBy ?taxon.
	?taxon skos:prefLabel ?sc_name.
        OPTIONAL {
                 ?taxon skos:altLabel ?synonym.
                 FILTER ( lang(?synonym) = "fr" )
                 }
	FILTER ( lang(?sc_name) = "fr" )
} 
LIMIT 50
```
### CQ: What are the fungi, their scientific names and vernacular names
Quels sont les champignons, leurs noms scientifiques et vernaculaires s'ils existent

```
SELECT DISTINCT ?fungi ?vernacular ?sc_name ?synonym
{ 
	?fungi rdf:type bago:Fungi.
	 OPTIONAL {
	          ?fungi skos:altLabel ?vernacular.
	           FILTER ( lang(?vernacular) = "fr" )
	           }
	?fungi bago:isDescribedBy ?taxon.
	?taxon skos:prefLabel ?sc_name.
        OPTIONAL {
                 ?taxon skos:altLabel ?synonym.
                 FILTER ( lang(?synonym) = "fr" )
                 }
	FILTER ( lang(?sc_name) = "fr" )
       
} 
LIMIT 50
```

### CQ: What are the animals, their scientific names and vernacular names
Quels sont les animaux, leurs noms scientifiques et vernaculaires s'ils existent

```
SELECT DISTINCT ?animal ?vernacular ?sc_name ?synonym
{ 
	?animal rdf:type bago:Animal.
	OPTIONAL {
	          ?animal skos:altLabel ?vernacular.
	           FILTER ( lang(?vernacular) = "fr" )
	           }
	?animal bago:isDescribedBy ?taxon.
	?taxon skos:prefLabel ?sc_name.
        OPTIONAL {
                 ?taxon skos:altLabel ?synonym.
                 FILTER ( lang(?synonym) = "fr" )
                 }
	FILTER ( lang(?sc_name) = "fr" )
} 
LIMIT 50
```
idée reprendre le sparql endpoint de wimmics mais il n'a pas utilisé les même identifiants.
http://purl.bioontology.org/ontology/NCBITAXON/2759 --> utilisé par nous et dans les ontoportals
http://purl.obolibrary.org/obo/NCBITaxon_2759 --> utilisé par wimmics


% pour aller interroger les sources distantes mais un organismes est typés par tous ses parents

```
SELECT DISTINCT ?org ?org_label ?taxon_URI ?taxon_label
WHERE 
{ 
?org skos:prefLabel ?org_label.
?org bago:isDescribedBy ?taxon_ind.
?taxon_ind rdf:type ?taxon_URI.
OPTIONAL 
   {
   SERVICE <https://taxref.i3s.unice.fr/sparql> 
      {
      ?taxon_URI rdfs:label ?taxon_label.
      ?taxon_URI <http://taxref.mnhn.fr/lod/property/hasRank> <http://taxref.mnhn.fr/lod/taxrank/Species>.
      }
   }
OPTIONAL
   {
   SERVICE <http://d2kab.i3s.unice.fr/sparql>
      { 
      
      GRAPH <http://purl.obolibrary.org/obo/ncbitaxon/ncbitaxon.owl>
        {
         BIND (IRI(replace(str(?taxon_URI),"http://purl.bioontology.org/ontology/NCBITAXON/", "http://purl.obolibrary.org/obo/NCBITaxon_")) AS ?new_URI).
	?new_URI rdfs:label ?taxon_label.
        }  
      }
   }
} 
LIMIT 10
```




```
SELECT DISTINCT ?org_label ?species_label
WHERE 
{ 
?org skos:prefLabel ?org_label.
?org bago:isDescribedBy ?taxon_ind.
?taxon_ind rdf:type ?taxon_URI.
   SERVICE <https://taxref.i3s.unice.fr/sparql> 
      {
      ?taxon_URI rdfs:label ?species_label.
      ?taxon_URI <http://taxref.mnhn.fr/lod/property/hasRank> <http://taxref.mnhn.fr/lod/taxrank/Species>.
      }

}
LIMIT 50
```
faire la meme chose pour NCBI taxon




```
SELECT DISTINCT ?org_pref_label ?org_alt_label ?vernacular_name
WHERE 
{ 
?org skos:prefLabel ?org_pref_label.
?org skos:altLabel ?org_alt_label.
?org bago:isDescribedBy ?taxon_ind.
?taxon_ind rdf:type ?taxon_URI.
   SERVICE <https://taxref.i3s.unice.fr/sparql> 
      {
      ?taxon_URI rdfs:label ?species_label.
      ?taxon_URI <http://taxref.mnhn.fr/lod/property/hasRank> <http://taxref.mnhn.fr/lod/taxrank/Species>.
      ?taxon_URI <http://taxref.mnhn.fr/lod/property/vernacularName> ?vernacular_name.
      FILTER ( lang(?vernacular_name) = "fr" )
      }

}
LIMIT 100
```
## Information about Grapevine Aggressions and their Harmful Organisms

CQ: Which cultivated plant C is attacked by the bioagressor B ? 
"cultivated grapevine" is attacked by "grape phylloxera", etc. 

CQ: What is the role of living organism O involved in the bioagressor attack A ? 
"Vitis vinifera" is the host, "Daktulosphaira vitifoliae" is the agressor, etc.

CQ: What is the type of bioagressors B ? 
"Daktulosphaira vitifoliae" is a pest macro organism, etc.
show the pathogen by types (virus, etc..)

### What are the Grapevine Aggressions that involved Bacterial Pathogens

```
SELECT DISTINCT ?ag ?ag_def ?dis_label ?bacterium_label ?vect_label
WHERE 
{ 
	?ag rdf:type bago:PlantAggressionSpecification.
	?ag skos:definition ?ag_def.
	OPTIONAL {
	          ?ag bago:hasDisease ?dis.
	          ?dis skos:prefLabel ?dis_label.
	           FILTER ( lang(?dis_label) = "fr" )
	           }
	?ag bago:hasAggressorAgent ?harm.
	?harm rdf:type bago:BacterialPathogenOrganism.
	?harm skos:prefLabel ?bacterium_label.
	FILTER ( lang(?bacterium_label) = "fr" )
	OPTIONAL {
	          ?ag bago:hasVectorAgent ?vect.
	          ?vect skos:prefLabel ?vect_label.
	          FILTER ( lang(?vect_label) = "fr" )
	          }           
}
LIMIT 50
```

### What are the Grapevine Aggressions that involved Fungus Pathogens

```
SELECT DISTINCT ?ag ?ag_def ?dis_label ?fungi_label ?vect_label
WHERE 
{ 
	?ag rdf:type bago:PlantAggressionSpecification.
	?ag skos:definition ?ag_def.
	OPTIONAL {
	          ?ag bago:hasDisease ?dis.
	          ?dis skos:prefLabel ?dis_label.
	           FILTER ( lang(?dis_label) = "fr" )
	           }
	?ag bago:hasAggressorAgent ?harm.
	?harm rdf:type bago:FungalPathogenOrganism.
	?harm skos:prefLabel ?fungi_label.
	FILTER ( lang(?fungi_label) = "fr" )
	OPTIONAL {
	          ?ag bago:hasVectorAgent ?vect.
	          ?vect skos:prefLabel ?vect_label.
	          FILTER ( lang(?vect_label) = "fr" )
	          }           
}
LIMIT 50
```


### What are the Grapevine Aggressions that involved Virus Pathogens

```
SELECT DISTINCT ?ag ?ag_def ?dis_label ?virus_label ?vect_label
WHERE 
{ 
	?ag rdf:type bago:PlantAggressionSpecification.
	?ag skos:definition ?ag_def.
	OPTIONAL {
	          ?ag bago:hasDisease ?dis.
	          ?dis skos:prefLabel ?dis_label.
	           FILTER ( lang(?dis_label) = "fr" )
	           }
	?ag bago:hasAggressorAgent ?harm.
	?harm rdf:type bago:ViralPathogenOrganism.
	?harm skos:prefLabel ?virus_label.
	FILTER (lang(?virus_label) = "fr" )
	OPTIONAL {
	          ?ag bago:hasVectorAgent ?vect.
	          ?vect skos:prefLabel ?vect_label.
	          FILTER ( lang(?vect_label) = "fr" )
	          }           
}
LIMIT 50
```

### What are the Grapevine Aggressions that involved Animal Pests 

```
SELECT DISTINCT ?ag ?ag_def ?dis_label ?animal_label ?vect_label
WHERE 
{ 
	?ag rdf:type bago:PlantAggressionSpecification.
	?ag skos:definition ?ag_def.
	OPTIONAL {
	          ?ag bago:hasDisease ?dis.
	          ?dis skos:prefLabel ?dis_label.
	           FILTER ( lang(?dis_label) = "fr" )
	           }
	?ag bago:hasAggressorAgent ?harm.
	?harm rdf:type bago:PestAnimal.
	?harm skos:prefLabel ?animal_label.
	FILTER (lang(?animal_label) = "fr" )
	OPTIONAL {
	          ?ag bago:hasVectorAgent ?vect.
	          ?vect skos:prefLabel ?vect_label.
	          FILTER ( lang(?vect_label) = "fr" )
	          }           
}
LIMIT 50

```

### What are the Pest Animal that transport also Pathogens ? 

```
SELECT DISTINCT ?animal_label ?pathogen_label
WHERE 
{ 
	
	?harm rdf:type bago:PestAnimal.
	?harm skos:prefLabel ?animal_label.
	FILTER (lang(?animal_label) = "fr" )
	?harm bago:transport ?pathogen.
	?pathogen skos:prefLabel ?pathogen_label.
	FILTER (lang(?pathogen_label) = "fr" )
     
}
LIMIT 50

```



## Information about Grapevine Diseases

CQ: What are the diseases related to grapevine ? 
example: diseases related to "cultivated grapevine" are phylloxera disease, mildew disease, etc.
show the disease by grouped biotic/abiotic, fungal / viral / ...)
Quelles sont les maladies de la vigne, organisées par type de maladie (fongique, bacterienne, virale, non parasitaire, abiotic)

CQ: What are the fungal diseases of grapevine ? 
Quelles sont les maladies de la vigne (vigne definie selon le thesaurus FCU) présentation en français

CQ: Which pathogen causes the disease D ? 
The phylloxera disease is caused by the "Daktulosphaira vitifoliae", etc.

CQ: What is the disease classe organised by pathogen types the disease belongs to? 
phylloxera disease is a pest disease, etc.

### What are fungal diseases of grapevine
Quelles sont les maladies fongiques de la vigne (vigne definie selon le thesaurus FCU)

```
SELECT DISTINCT ?dis ?dis_name ?dis_desc 
WHERE 
{ 
	?dis a bago:FungalPlantDisease.
	?dis skos:prefLabel ?dis_name.
	?dis skos:definition ?dis_desc.
	?dis bago:isDiseaseOf ?ag.
	?ag bago:hasHostAgent ?plant.
	?plant fcuo:hasUsage fcu:Vignes_cultivees.
	filter langMatches(lang(?dis_name), "fr")
} 
ORDER BY ?dis_name
```

### What are viral diseases of grapevine
Quelles sont les maladies virales de la vigne

```
SELECT DISTINCT ?dis ?dis_name ?dis_desc 
WHERE 
{ 
	?dis a bago:ViralPlantDisease.
	?dis skos:prefLabel ?dis_name.
	?dis skos:definition ?dis_desc.
	?dis bago:isDiseaseOf ?ag.
	?ag bago:hasHostAgent ?plant.
	?plant fcuo:hasUsage fcu:Vignes_cultivees.
	filter langMatches(lang(?dis_name), "fr")
} 
ORDER BY ?dis_name
```

### What are bacterial diseases of grapevine
Quelles sont les maladies bactériennes de la vigne

```
SELECT DISTINCT ?dis ?dis_name ?dis_desc 
WHERE 
{ 
	?dis a bago:BacterialPlantDisease.
	?dis skos:prefLabel ?dis_name.
	?dis skos:definition ?dis_desc.
	?dis bago:isDiseaseOf ?ag.
	?ag bago:hasHostAgent ?plant.
	?plant fcuo:hasUsage fcu:Vignes_cultivees.
	filter langMatches(lang(?dis_name), "fr")
} 
ORDER BY ?dis_name
```

### What are no parasitic biotic diseases of grapevine 
Quelles sont les maladies biotiques non parasitaire de la vigne

```
SELECT DISTINCT ?dis ?dis_name ?dis_desc 
WHERE 
{ 
	?dis a bago:NonParasiticBioticPlantDisease.
	?dis skos:prefLabel ?dis_name.
	?dis skos:definition ?dis_desc.
	?dis bago:isDiseaseOf ?ag.
	?ag bago:hasHostAgent ?plant.
	?plant fcuo:hasUsage fcu:Vignes_cultivees.
	filter langMatches(lang(?dis_name), "fr")
} 
ORDER BY ?dis_name
```
### What are abiotic diseases of grapevine
Quelles sont les maladies abiotiques de la vigne

```
SELECT DISTINCT ?dis ?dis_name ?dis_desc 
WHERE 
{ 
	?dis a bago:AbioticPlantDisease.
	?dis skos:prefLabel ?dis_name.
	?dis skos:definition ?dis_desc.
	?dis bago:isDiseaseOf ?ag.
	?ag bago:hasHostAgent ?plant.
	?plant fcuo:hasUsage fcu:Vignes_cultivees.
	filter langMatches(lang(?dis_name), "fr")
} 
ORDER BY ?dis_name
```

## Information about Symptoms


CQ: Which parts of the cultivated plant C present some symptoms of the bioagressor attack? 
"Daktulosphaira vitifoliae" attacks the leaves and the roots, etc.

CQ: What is the disease classe organised by organs attacked the disease belongs to? 
phylloxera disease is a root disease and an leave disease, etc.


```
SELECT DISTINCT ?ag ?ag_def ?symptom_label ?organ_label
WHERE 
{ 
	?ag rdf:type bago:PlantAggressionSpecification.
	?ag skos:definition ?ag_def.
	OPTIONAL {
	          ?ag skos:note ?symptom_label.
	          FILTER ( lang(?symptom_label) = "fr" )
	          FILTER ( contains(?symptom_label, "Sympt") )
	           }
	OPTIONAL {
	          ?ag skos:note ?organ_label.
	          FILTER ( lang(?organ_label) = "fr" )
	          FILTER ( contains(?organ_label, "Organes atteints") )
	           }
	
}
LIMIT 50
```

