# Knowledge Graph on BioAGgressor of Grapevine (BAG_GV)

The Knowledge Graph on BioAGgressor of GrapeVine (BAG-GV) was build during the D2KAB project. The goal is to model all the living organisms involved in grappevine disease appearance and pests. The graph and its documentation represent the point of view of some french researchers from differents institutions: French Institute of Vine and Grapevine (IFV), INRAE.


The French ANR project "Data to Knowledge in Agronomy and Biodiversity" D2KAB (www.d2kab.org) has created a framework to turn agronomy and biodiversity data into knowledge –semantically described, interoperable, actionable, open– and investigate scientific methods and tools to exploit this knowledge for applications in science & agriculture. One of the five driving scenarii of D2KAB was to publish on the Web of Linked data an archive of French agricultural alert bulletins, called Plant Health Bulletins (Bulletins de Santé du Végétal). To annotate the bulletins using crop diseases and pests, we need a new semantic resource. Several ontologies and knowledge graphs already exist on the subject, but they do not cover all our needs. We have therefore developed a new knowledge graph about "BioAGgressor of GrapeVine" (BAG-GV), populating the BioAGgressor Ontology (BAGO). This new knowledge graph was developed using the results of entity extraction tool AlvisNLP applied on a set of Plant Health Bulletins of grapevine. The KG was then validated by human experts.

The file bag_gv_latest.owl contains the KG on bioaggressor of grapevine.

## Access Description and Identifier

The licence is CC BY-ND 4.0 https://creativecommons.org/licenses/by-nc-nd/4.0/deed.fr
due to the fact that we present some definitions proposed by Quae Books.

BAG_GV URI is https://opendata.inrae.fr/bag-res/grapevine/

BAG-GV is available on :
 * AgroPortal https://agroportal.lirmm.fr/ontologies/BAG-GV
 * Research Data Gouv: ROUSSEY, Catherine; SAUVION, Nicolas; LARIGNON, Philippe; DELPUECH, Xavier, 2024, "Grapevine Bioaggressor Knowledge Graph", https://doi.org/10.57745/JEZBJW, Recherche Data Gouv, V1
 * SPARQL EndPoint : https://rdf.codex.cati.inrae.fr/bag-res/grapevine/sparql

## Developement Process

The KG was populated based on entity extraction NLP processes from Plant Health Corpus named "Bulletin de Santé du Végétal".
The KG was enriched by reasoning in order to infer the type of organisms and the type of diseases.

### Sources of information
The reference sources of information used during the conceptualisation of BAGO and grapevine disease KG are:
* the INRAE web site ephytia: https://ephytia.inra.fr/fr/
* the INRAE thesaurus https://thesaurus.inrae.fr/thesaurus-inrae/fr/
* the IFV web page called "fiches pratiques" of the IFV Occitanie Center https://www.vignevin-occitanie.com/fiches-pratiques/
* Le larousse agricole 1981
* the book Phil Taylor. Plantwise Diagnostic Field Guide: A tool to diagnose crop problems and make recommendations for their management, www.plantwise.org, CABI, 2015, 118 pages, https://www.plantwise.org/wp-content/uploads/sites/4/2019/11/Diagnostic-Field-Guide.pdf
* the book Pierre-Henri Dubuis (coordination), Aurélie Gfeller, Lina Künzler, Patrik Kehrli, Christian Linder, Jean-Sébastien Reynard, Christophe Debonneville, Jean-Laurent Spring, Vivian Zufferey, Kathleen Mackie-Haas. Guide phytosanitaire pour la viticulture 2021–2022. Agroscope Transfer N°370 janvier 2021. https://www.ne.ch/autorites/DDTE/SAGR/viticulture/Documents/Guide%20phytosaniraire%20pour%20la%20viticulture%202021-2022.pdf
* the book second edition of the OIV descriptor list for grape varieties and vitis species, Organisation Internationale de la Vigne et du Vin (OIV), 232 pages, 2022, https://www.oiv.int/sites/default/files/2022-12/Code%202e%20edition%20Finale_1.pdf
* Jean-François Soussana (coordination scientifique). S'adapter au changement climatique", Edition QUAE, 9 septembre 2023, 296 pages, https://www.quae.com/produit/1199/9782759221066/s-adapter-au-changement-climatique, EAN13 eBook [PDF] : 9782759220175

## Authors and contributors

Catherine ROUSSEY, researcher at MISTEA INRAE Occitanie Center Montpellier (https://orcid.org/0000-0002-3076-5499): She is a Semantic Web expert. She is in charged of the CHOWLK conceptualiaation, the formalisation using Protégé Tool and the documentation.

A pool of experts  was interviewed during the conceptualization of BAGO and grapewine disease KG :
* Nicolas SAUVION, PHIM, INRAE Occitanie Center Montpellier(https://orcid.org/0000-0002-1641-5871): he is an entomologist researcher. He validate the conceptualization. He provides some textual definitions and sources of information.
* Philippe LARIGNON, IFV (https://www.vignevin-occitanie.com/qui-sommes-nous/pole-rhone-mediterranee/philippe-larignon/): he is an expert in grapevine wood disease. He validates the label of diseases and the scientific name of pathogens.
* Xavier DELPUECH, IFV, (https://orcid.org/0000-0001-7278-3209): he provides some sources for grapevine diseases. He validates the knowledge graph.

The grapevine disease KG was build thanks to NLP process designed by :
* Marine COURTIN, TSCF & MAIAGE INRAE (https://orcid.org/0000-0003-4189-4322): She is an NLP research engineer. She defines the NLP processes to extract the bioaggressors from Plant Health Bulletins.
* Robert BOSSY, MAIAGE INRAE Center Jouy-en-Josas Antony (https://orcid.org/0000-0001-6652-9319): He is an NLP research engineer and has some experience in taxonomy. He design the NLP framework used by Marine.

The grapevine disease KG is aligned with INRAE thesaurus.
* Sophie AUBIN, DIPSO, INRAE (https://orcid.org/0000-0003-4805-8220): she manages the discussions between INRAE thesaurus managers and BAGO creator and contributors in order to propose some alignements between those semantic resources.

Some engineers published the RDF graphs on the web:
* Stephan BERNARD, LISC INRAE Center ARA Clermont-Ferrand (https://orcid.org/0000-0001-9694-1443): He is a Semantic Web engineer involved in the CATI CODEX. He is in charge of the grapevine disease KG publication in SPARQL Endpoint of the CATI CODEX.

Productor:
* National research institute for agriculture food and environment (INRAE) [https://www.inrae.fr/], ror:[https://ror.org/003vg9w96]
* Mathématiques, Informatique et Statistique pour l'Environnement et l'Agronomie (MISTEA) [https://www6.montpellier.inra.fr/mistea_eng/], ror: [https://ror.org/01pd2sz18]
* Institut Français de la Vigne et du Vin (IFV) [http://www.vignevin.com/], ror: [https://ror.org/03synvw58]

Funding:
* ANR-18-CE23-0017 D2KAB (Data to Knowledge in Agronomy and Biodiversity) http://www.d2kab.org . WP T4.3 BSV Reader
* French National Projet call CASDAR Connaissances 2024: Standardiser les données expérimentales et Techniques pour faciliter leur réutilisation et accélérer l’innovation et le développement agricole : application aux travaux sur les biosolutions (STAR)

## Documentation

### Specification: Competency Questions (CQ).

* CQ1: What is the scientific taxon family of the living organism O (bioaggressor or cultivated plant)? 
"Daktulosphaira vitifoliae" is an insect, "grapevine" is a shrub woody plant,  etc.
* CQ2: What is the scientific name (species) of the living organism O (bioaggressor or cultivated plant)? 
cultivated grapevine species is "Vitis vinifera", grape phylloxera species is "Daktulosphaira vitifoliae", etc.
* CQ3: What is the vernacular name of the living organism  O (bioagressor or cultivated plant)? 
"Vitis vinifera" vernacular name is "cultivated grapevine", "Daktulosphaira vitifoliae" vernacular name is "grape phylloxera", etc.
* CQ4: What is the role of living organism O involved in the bioagressor attack A ? 
"Vitis vinifera" is the host, "Daktulosphaira vitifoliae" is the agressor, etc.
* CQ5: What is the type of bioagressors B ? 
"Daktulosphaira vitifoliae" is a pest macro organism, etc.
* CQ6: Which cultivated plant C is attacked by the bioagressor B ? 
"cultivated grapevine" is attacked by "grape phylloxera", etc. 
* CQ7: What are the diseases related to grapevine ? 
example: diseases related to "cultivated grapevine" are phylloxera disease, mildew disease, etc.
* CQ8: Which pathogen causes the disease D ? 
The phylloxera disease is caused by the "Daktulosphaira vitifoliae", etc.
* CQ9: Which parts of the cultivated plant C present some symptoms of the bioagressor attack? 
"Daktulosphaira vitifoliae" attacks the leaves and the roots, etc.
* CQ10: What is the disease classe organised by organs attacked the disease belongs to? 
phylloxera disease is a root disease and an leave disease, etc.
* CQ11: What is the disease classe organised by pathogen types the disease belongs to? 
phylloxera disease is a pest disease, etc.



