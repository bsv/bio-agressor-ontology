# BioAGgressor knowledge graph for GrapeVine (BAG-GV) creation method using cellfie

bag_gv_vide.owl contains the metadata description of knowledge graph on grapevine bioaggressors.

Delete the catalog-v001.xml file to remove previous references to any semantic ressources in Protege.

This empty KG contains the description of one organism that represents a grapevine plant. This organism is described by FCU concepts.


## Genetic Ressource Classes Creation

Change the preferences in Protege of renderer as entity IRI short name


### Create the taxons from NCBI taxons
Change the preferences in Protege of new entity
	Entity Iri
	 	start with specified IRI: http://purl.bioontology.org/ontology/NCBITAXON
	 	followed by : /

Create the class using cellfie with the file graphe_Bioagresseurs_vigne.xls and the NcbiTaxonClassCreation rule.
create the class hierarchy using cellfie with the file graphe_Bioagresseurs_vigne.xls and the NcbiTaxonClassHierarchyCreation rule.

skos:definition @D*(xml:lang="en" mm:prepend(@C*(mm:append(" "))))
### Create the taxons from TAXREF-LD
Change the preferences in Protege
	Entity Iri
	 	start with: http://taxref.mnhn.fr/lod/taxon
	 	followed by : /

Create the class using cellfie with the file graphe_Bioagresseurs_vigne.xls and the TaxrefClassCreation rule.
create the class hierarchy using cellfie with the file graphe_Bioagresseurs_vigne.xls and the TaxrefClassHierarchyCreation rule.

The FCUO class Crop is already defined in BAGO.
the skos:Instance about grapevine already exist in bag_gv_vide.owl

When a new specie should be defined (related to a plant for example), the genus, family, order and class taxon should also be defined in the taxon tab.

un individu taxon d des labels preferés dans les deux langues avec le nom scientifique et leur synonyme.
un organisme a comme labels preferes le nom scientifique dans les deux langues et comme labels alternatifs les noms vernaculaires dans les deux langues (si possible).


### Plant Individuals Creation

check that the individual organism_grapevine already exist in bag_gv_vide.owl


Create the new individual taxon taxon_sp_vitis_vinifera
Change the preferences in Protege of new entity
	Entity Iri
	 	start with specified IRI: Active Ontology IRI
	 	followed by : /
Create the new taxon individual using cellfie with the file graphe_Bioagresseurs_vigne.xls and the PlantTaxonIndividualCreation rule.

Note to modify the uri taxon_sp_Vitis_vinifera change in taxon_sp_vitis_vinifera (remove the uppercase)

use SWRL Tab to infer inverse object property links.
swrl table 5

save the ontology (generate too much inferences at the same time, block protégé tool)
use another ontology name to be sure to follow the process.

## Aggression Specification Individual Creation

une aggression est une relation N-aire

some aggressions are kind of generic one AGD012 "jaunisses de la vigne" and AGD016 "pourridié des racines" and are not described completely, thus they are defined with skos:narrower link to more precise aggressions.

### Disease Aggression Specification Individual Creation

Create Individual of Plant Specification Aggression

Check the preferences in Protege of new entity
	Entity Iri
	 	start with specified IRI: Active Ontology IRI
	 	followed by : /
Create the new individual using cellfie with the file graphe_Bioagresseurs_vigne.xls and the DiseaseAggressionIndividualCreation rule.

create some kind of hierarchy (skos:broader) between aggression specification using cellfie with the file graphe_Bioagresseurs_vigne.xls and the DiseaseAggressionHierarchyCreation rule.

use SWRL Tab to infer inverse object property links.
swrl table 5

save the ontology

### Disease Individual Creation

Create Individual of Plant Disease

Check the preferences in Protege of new entity
	Entity Iri
	 	start with specified IRI: Active Ontology IRI
	 	followed by : /
Create the new Disease individual using cellfie with the file graphe_Bioagresseurs_vigne.xls and the DiseaseIndividualCreation rule.

create some kind of hierarchy (skos:broader) between Diseases using cellfie with the file graphe_Bioagresseurs_vigne.xls and the DiseaseHierarchyCreation rule.

Create the link between the aggression and the disease using cellfie with the file graphe_Bioagresseurs_vigne.xls and the AgToDiseaseCreationLink rule.

use SWRL Tab to infer inverse object property links.
swrl table 5

save the ontology

### Pest Aggression Specification Individual Creation

Create Individual of Plant Specification Aggression

Check the preferences in Protege of new entity
	Entity Iri
	 	start with specified IRI: Active Ontology IRI
	 	followed by : /
Create the new individual using cellfie with the file graphe_Bioagresseurs_vigne.xls and the PestAggressionIndividualCreation rule.

create some kind of hierarchy (skos:broader) between aggression specification using cellfie with the file graphe_Bioagresseurs_vigne.xls and the PestAggressionHierarchyCreation rule.

use SWRL Tab to infer inverse object property links.
swrl table 5

save the ontology

## Organism and Taxon Individual Creation

dans le taxon le laben préféré est le nom scientifique de reference en anglais et francais , le label alternatif est le synonyme (ancien nom scientifique connu par les experts)

dans l'organisme le label préféré est le nom scientifiques de la taxonomie source et les labels alternatifs sont les noms vernaculaires en francais

### Vector Individual Creation

Create the new taxon individual using cellfie with the file graphe_Bioagresseurs_vigne.xls and the VectorTaxonIndividualCreation rule.

Create the new organism individual using cellfie with the file graphe_Bioagresseurs_vigne.xls and the VectorOrganismIndividualCreation rule.

	
use SWRL Tab to infer inverse object property links.
use SWRL tab to infer subClassOf links (table 7 CAX_SCO rules)

save the ontology

### Pathogen Individual Creation

Create the new taxon individual using cellfie with the file graphe_Bioagresseurs_vigne.xls and the PathogenTaxonIndividualCreation rule.

Note that when a taxon has different scientific names in TAXREF and NCBI, then the same taxon individual is instance of two classes. Check if the synonyme scientific name is the alternative label of the taxon individual.

Dans le taxon je ne mets que les noms scientifiques préférés et synonymes


Create the new organism individual using cellfie with the file graphe_Bioagresseurs_vigne.xls and the PathogenOrganismIndividualCreation rule.
The organism individual is described by one unique taxon individual.

use SWRL Tab to infer inverse object property links.
use SWRL tab to infer subClassOf links (table 7 CAX_SCO rules)

save the ontology

### Pest Individual Creation

certains ravageurs (pests) sont aussi vecteurs, il faut trier l'onglet pests et ne declarer que les ravageurs qui n'existent pas encore dans les vecteurs.

Create the new taxon individual using cellfie with the file graphe_Bioagresseurs_vigne.xls and the PestTaxonIndividualCreation rule.

Create the new organism individual using cellfie with the file graphe_Bioagresseurs_vigne.xls and the PestOrganismIndividualCreation rule.

use SWRL Tab to infer inverse object property links.
use SWRL tab to infer subClassOf links (table 7 CAX_SCO rules)

save the ontology

### Organism to Aggression

Create the link between the aggression and the pathogen using cellfie with the file graphe_Bioagresseurs_vigne.xls and the AgToAggressorCreationLink rule.
	
use SWRL Tab to infer inverse object property links.
use SWRL tab to infer subClassOf links (table 7 CAX_SCO rules)


## inferences
	
use SWRL Tab to infer inverse object property links.
use SWRL tab to infer subClassOf links (table 7 CAX_SCO rules)

normalement ca aété fait pas à pas pour eviter la surcharge du raisonneur

domain rules are saved in SwrlRules.txt files.
Protege renderer should be fixed to prefix name.

use SWRL tab and domain rules to infer tansport, attack, "affect", cause, contract, transmitTheCauseOf links and its inverse links. 
Take attention to the axiom's  order, order has importance.
check that the inferences are complete using the reasoner Hermit. Sometimes the domain rules should be run several times.

use SWRL tab and domain rules to infer Animal, Fungi, Bacterium, Virus, Plant, BioticVector, Pathogens, Pest class instances.

### final notes

dans l'onglet Notes on peut rajouter des skos:note sur certains elements de l'ontologie.

note that NCBI  has not a description of grapevine leafroll associated virus 2 and  grapevine leafroll associated virus


